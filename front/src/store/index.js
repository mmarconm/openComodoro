import Vue from 'vue'
import Vuex from 'vuex'
import axios from '../plugins/axios'
import createPersistedState from 'vuex-persistedstate'

Vue.use(Vuex)

const state = {
  user: null,
  token: null,
  equipes: [],
  academias: [],
  professores: [],
  configuracoes: [],
  inscricoes: [],
  professor: null
}

const mutations = {
  LOGOUT (state) {
    state.user = null
    state.token = null
    state.equipes = []
    state.academias = []
    state.professores = []
    state.configuracoes = []
    state.inscricoes = []
    state.professor = null
  },
  AUTH (state, payload) {
    state.user = payload
  },
  LOGIN (state, payload) {
    state.user = payload.user
    state.token = payload.token
  },
  REMOVE: (state, payload) => {
    state[payload.state].forEach((item, index) => {
      if (item.id === payload.data) {
        state[payload.state].splice(index, 1)
      }
    })
  },
  APPLY: (state, payload) => {
    if (Array.isArray(payload.data)) {
      state[payload.state] = payload.data
    } else {
      if (Array.isArray(state[payload.state])) {
        let update = state[payload.state].filter(
          f => f.id === payload.data.id
        ).length > 0
        if (update) {
          state[payload.state] = state[payload.state].map(
            m => m.id === payload.data.id ? payload.data : m
          )
        } else {
          state[payload.state].push(payload.data)
        }
      } else {
        state[payload.state] = payload.data
      }
    }
  }
}

const parseData = (context, {commit, state}, data) => {
  context.commit('APPLY', {state, data})
}

const removeData = (context, {commit, state}, data) => {
  context.commit('REMOVE', {state, data})
}

const actions = {
  load: (context, payload) => axios.get(payload.url, payload.fiters)
    .then(data => parseData(context, payload, data.data)),
  new: (context, payload) => axios.post(payload.url, payload.data)
    .then(data => parseData(context, payload, data.data)),
  update: (context, payload) => axios.patch(`${payload.url}${payload.data.id}/`, payload.data)
    .then(data => parseData(context, payload, data.data)),
  remove: (context, payload) => {
    axios.delete(`${payload.url}${payload.data.id}/`)
      .then(data => removeData(context, payload, payload.data.id))
  },
  login: (context, payload) => axios.post('rest-auth/login/', payload).then(
    data => {
      context.commit('LOGIN', data.data)
    }
  ),
  logout: (context, payload) => axios.post('rest-auth/logout/').then(
    () => {
      context.commit('LOGOUT')
      location.reload()
    }
  ).catch(
    () => {
      context.commit('LOGOUT')
      location.reload()
    }
  ),
  check: (context, payload) => new Promise((resolve, reject) => {
    if (context.state.token !== null) {
      axios.defaults.headers.common['Authorization'] = 'JWT ' + context.state.token
      axios.get('rest-auth/user/').then(
        data => {
          context.commit('AUTH', data.data)
          resolve()
        }
      ).catch(
        error => {
          context.commit('LOGOUT')
          reject(error.response.data)
        }
      )
    } else {
      throw new Error('Não foi localizado chave de acesso. Efetue login novamente')
    }
  })
}

export default new Vuex.Store({
  plugins: [createPersistedState({
    key: 'openComodoro'
  })],
  state,
  mutations,
  actions
})
