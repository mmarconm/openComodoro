import Vue from 'vue'
import Login from './Login'
import Countdown from './Countdown'
import EquipeForm from './EquipeForm'
import AcademiaForm from './AcademiaForm'
import AtletaForm from './AtletaForm'
import ProfessorForm from './ProfessorForm'

Vue.component('login', Login)
Vue.component('countdown', Countdown)
Vue.component('form-academia', AcademiaForm)
Vue.component('form-equipe', EquipeForm)
Vue.component('form-atleta', AtletaForm)
Vue.component('form-professor', ProfessorForm)
