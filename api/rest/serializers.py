from rest_framework import serializers

from core.models import *


class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = '__all__'
        read_only_fields = ('email', )

class EquipeSerializer(serializers.ModelSerializer):
    class Meta:
        fields = '__all__'
        model = Equipe


class AcademiaSerializer(serializers.ModelSerializer):
    equipe_display = serializers.SerializerMethodField()

    def get_equipe_display(self, obj):
        return obj.equipe.nome

    class Meta:
        fields = '__all__'
        model = Academia


class ProfessorSerializer(serializers.ModelSerializer):
    equipe_display = serializers.SerializerMethodField()
    academia_display = serializers.SerializerMethodField()

    def get_equipe_display(self, obj):
        return obj.academia.equipe.nome

    def get_academia_display(self, obj):
        return obj.academia.nome

    class Meta:
        fields = '__all__'
        model = Professor


class InscricaoSerializer(serializers.ModelSerializer):
    sexo_display = serializers.SerializerMethodField()
    categoria_idade_display = serializers.SerializerMethodField()
    faixa_display = serializers.SerializerMethodField()
    categoria_peso_display = serializers.SerializerMethodField()
    professor_display = serializers.SerializerMethodField()

    def get_professor_display(self, obj):
        return obj.professor.nome

    def get_sexo_display(self, obj):
        return obj.get_sexo_display()

    def get_categoria_idade_display(self, obj):
        return obj.get_categoria_idade_display()

    def get_faixa_display(self, obj):
        return obj.get_faixa_display()

    def get_categoria_peso_display(self, obj):
        return obj.get_categoria_peso_display()

    class Meta:
        fields = '__all__'
        model = Inscricao
