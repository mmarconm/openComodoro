from django.contrib.auth.models import User
from django.db import models


class Equipe(models.Model):
    nome = models.CharField(max_length=100)
    observacao = models.TextField(null=True, blank=True)
    logo = models.ImageField(upload_to='equipes', null=True)

    def __str__(self):
        return self.nome


class Academia(models.Model):
    nome = models.CharField(max_length=100)
    cidade = models.CharField(max_length=100)
    estado = models.CharField(max_length=2)
    equipe = models.ForeignKey('core.Equipe', on_delete=models.CASCADE)

    def __str__(self):
        return self.nome


class Professor(models.Model):
    academia = models.ForeignKey('core.Academia', on_delete=models.CASCADE)
    nome = models.CharField(max_length=100)
    email = models.EmailField(null=True, blank=True)
    telefone = models.CharField(max_length=20, null=True)

    def save(self, *args, **kwargs):
        user = User.objects.filter(email=self.email).first()
        username = self.email.split('@')[0]
        if user is None:
            User.objects.create_user(username=username,
                                     email=self.email,
                                     password=f'{username}2018',
                                     first_name=self.nome,
                                     is_staff=True)
        else:
            user.username = username
            user.is_staff = True
            user.email = self.email
            user.save()

        super().save(*args, **kwargs)

    def __str__(self):
        return self.nome


class Inscricao(models.Model):
    SEXO = (
        ('M', 'Masculino'),
        ('F', 'Feminino'),
    )
    FAIXA = (
        (1, 'Branca'),
        (2, 'Cinza'),
        (3, 'Amarela'),
        (4, 'Laranja'),
        (5, 'Verde'),
        (6, 'Azul'),
        (7, 'Roxa'),
        (8, 'Marrom'),
        (9, 'Preta'),
    )
    CATEGORIA_PESO = (
        (1, 'Galo - Até 57,50 Kg'),
        (2, 'Pluma - Até 64 Kg'),
        (3, 'Pena - Até 76 Kg'),
        (5, 'Médio - Até 82,3 Kg'),
        (6, 'Meio Pesado - Até 88,30 Kg'),
        (7, 'Pesado - Até 94,30 Kg'),
        (8, 'Super Pesado - Até 100,50 Kg'),
        (9, 'Pesadíssimo'),
    )
    CATEGORIA_IDADE = (
        (1, 'Pré-Mirin - De 4 a 6 Anos'),
        (2, 'Mirin - De 7 a 9 Anos'),
        (3, 'Infantil - De 10 a 12 Anos'),
        (4, 'Infanto Juvenil - De 13 a 15 Anos'),
        (7, 'Juvenil - De 16 a 17 Anos'),
        (5, 'Adulto - Até 31 Anos'),
        (6, 'Master'),
    )

    data = models.DateTimeField(auto_now_add=True)
    nome = models.CharField(max_length=100)
    email = models.EmailField(null=True)
    telefone = models.CharField(max_length=20, null=True)
    sexo = models.CharField(max_length=1, choices=SEXO)
    categoria_idade = models.IntegerField('Idade', choices=CATEGORIA_IDADE, default=5)
    faixa = models.IntegerField(choices=FAIXA, default=1)
    categoria_peso = models.IntegerField('Peso', choices=CATEGORIA_PESO, default=1)
    absoluto = models.BooleanField(default=False)
    valor = models.DecimalField(max_digits=15, decimal_places=2, default=50, null=True, blank=True)
    valor_absoluto = models.DecimalField(max_digits=15, decimal_places=2, default=70, null=True, blank=True)
    pago_inscricao = models.BooleanField(default=False)
    pago_absoluto = models.BooleanField(default=False)
    professor = models.ForeignKey('core.Professor', on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'inscrição'
        verbose_name_plural = 'incrições'

    def __str__(self):
        return self.nome
